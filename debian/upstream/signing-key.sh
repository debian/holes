#!/usr/bin/env bash
set -euo pipefail

KEYS=(
  E6F6848A1B95EE313CF5B7EE95FF633C90A8F025 # Leah Neukirchen <leah@vuxu.org>
)

if [ $# -gt 0 ]; then
	exec gpg "$@" "${KEYS[@]}"
else
	exec gpg --export --export-options export-minimal -a --yes \
		-o "$(dirname "$0")/signing-key.asc" "${KEYS[@]}"
fi
